<?
require_once('../server_config.php');

define('DEFAULT_LANGUAGE_ID',	1);
define('DEFAULT_LANGUAGE',		'eng');
define('DEFAULT_CONTROLLER',	'home');
define('DEFAULT_ACTION',		'index');

$Path = [
	APP_ROOT.'conf',
	APP_ROOT.'core',
	APP_ROOT.'libs',
	APP_ROOT.'libs/traits',
	APP_ROOT.'libs/mailgun',
	APP_ROOT.'core/controllers',
	APP_ROOT.'core/models',
	APP_ROOT.'core/views',
	APP_ROOT.'core/views/inc',
	APP_ROOT.'core/traits',
	APP_ROOT.'core/services',
	APP_ROOT.'public',
	APP_ROOT.'public/css',
	APP_ROOT.'public/docs',
	APP_ROOT.'public/js',
	APP_ROOT.'public/fonts',
	APP_ROOT.'public/images',
];

// enable/disable stop page
define('SERVER_UPDATE',			false);
// MySQL logging constants
define('MYSQL_PROFILER',		false);
define('MYSQL_LOGGING',			false);
?>