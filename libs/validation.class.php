<?
// Validation class, handles all validations
class Validation
{
	const	NUMERIC	= '0-9';
	const	ALPHA	= 'a-zA-z';
	const	SPECIAL	= '-_+=';

	public	$Messages = [
		'Error_Empty_Fields'			=> 'Sorry, please fill out all required fields',
		'Error_Not_Matched'				=> 'Sorry, some fields don\'t match',
		'Error_User_Exists'				=> 'Sorry, user already exists',
		'Error_Wrong_Email'				=> 'Sorry, email domain is wrong',
		'Error_Not_Allowed_Characters'	=> 'Sorry, you used some not allowed characters',
		'Success_Order_Book'			=> 'Thank you! You will be redirected a few seconds.',
	];

	// constructor
	public function __construct()
	{}

	// method to validate if field is empty or not
	// @param	array	$Fields			- fields to be validated
	// @return	array	$ListOfErros	- list of all errors, or empty error if everything is ok
	public function ValidateEmptyFields($Fields)
	{
		$ListOfErrors = [];
		foreach ($Fields as $Field)
		{
			if (empty($_SESSION['data'][$Field])) {$ListOfErrors[] = $Field;}
		}

		return $ListOfErrors;
	}

	// method to validate if fields match to each or not
	// @param	array	$Fields			- fields to be validated
	// @return	array	$ListOfErrors	- list of all errors, or empty error if everything is ok
	public function ValidateMatchFields($Fields)
	{
		$ListOfErrors = [];
		$Flag = true;
//		$Value = current(array_values($_SESSION['data']));
		$Value = $_SESSION['data'][$Fields[0]];
		foreach ($Fields as $Field)
		{
			if ($Value != $_SESSION['data'][$Field]) {$Flag = false;}
		}
		if (!$Flag)
		{
//			$ListOfErrors[] = implode(' AND ', $Fields);
			$ListOfErrors = $Fields;
		}

		return $ListOfErrors;
	}

	// method to check if Name fields content only Alpha characters & '-'
	// @param	array	$Fields			- fields to be validated
	// @return	array	$ListOfErrors	- list of all errors, or empty error if everything is ok
	public function ValidateAllowedCharactersNames($Fields)
	{
		$ListOfErrors = [];
		$AllowedCharacters = self::ALPHA.' -';

		$Pattern = "/^[".$AllowedCharacters."]+$/i";
		foreach ($Fields as $Field)
		{
			preg_match($Pattern, $_SESSION['data'][$Field], $Result);
			if (count($Result) == 0) {$ListOfErrors[] = $Field;}
		}

		return $ListOfErrors;
	}
}
?>