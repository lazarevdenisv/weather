<?
// Trait to handle messaging system through the whole core
trait MessageTrait
{
	public $Message;
	public $CurrentMessage;
	public $MessageType;
	public $MessageDisplay;

	// generic constructor
	public function __construct()
	{
	}

	// method to reset messages
	public function ResetMessage()
	{
		$this->Message = $this->SetEmptyMessage();
		$this->SaveLastMessageInSession();

		return true;
	}

	// set an empty message
	private function SetEmptyMessage()
	{
		$Message = [
			'response'	=> '',
			'action'	=> false,
			'fields'	=> [],
			'message'	=> '',
		];

		return $Message;
	}

	// set default message to success
	public function SetDefaultMessageSuccess($Message=null)
	{
		$this->ResetMessage();
		$this->Message['response'] = 'success';
		$this->SetMessageAction(true);
		if (!is_null($Message)) {$this->Message['message'] = $Message;}

		return true;
	}

	// set default message to failed
	public function SetDefaultMessageFail($Message=null)
	{
		$this->ResetMessage();
		$this->Message['response'] = 'fail';
		$this->SetMessageAction(true);
		if (!is_null($Message)) {$this->Message['message'] = $Message;}

		return true;
	}

	// set action into the message
	public function SetMessageAction($Action=true)
	{
		$this->Message['action'] = $Action;

		return true;
	}

	// save the last message into session
	public function SaveLastMessageInSession()
	{
		$_SESSION['messages']['last'] = $this->Message;

		return true;
	}

	// get the last message from the session
	public function GetLastMessageFromSession()
	{
		$this->Message = (isset($_SESSION['messages']['last'])) ? $_SESSION['messages']['last'] : $this->SetEmptyMessage();

		return true;
	}
}
?>