<?
// trait to build singleton pattern
trait SingletonTrait
{
	// singleton instance
	private static $Instance = null;

	// protection against creating new instance
	private function __construct()
	{}

	// protection against cloning
	private function __clone()
	{}

	// protection against unserialize
	private function __wakeup()
	{}

	// method to get the existing instance
	static public function GetInstance()
	{
		static::$Instance = (empty(static::$Instance)) ? new static() : static::$Instance;

		return static::$Instance;
	}
}
?>