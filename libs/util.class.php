<?
// Util static class, contents all util functions for the core
class Util
{
	// function to decode Url and strip tags from it
	// @param	string	$Url	- url string to be encoded and parsed, striped-taged, etc.
	// @return	string	$Url	- parsed URL
	static public function GetURL($Url)
	{
		return strip_tags(urldecode($Url));
	}

	// function to encode Url
	// @param	string	$Url	- url string
	// @return	string	$Url	- encoded url string
	static public function SetURL($Url)
	{
		return urlencode($Url);
	}

    // function to strip and trim input field
    // @param    array    $Array        - array of fields to be parsed, trimmed, etc.
    // @return    array    $NewArray    - parsed array
    static public function GetArray($Array)
    {
        $NewArray = [];
        foreach ($Array as $Key => $Value)
        {
            $NewArray[$Key] = self::GetField($Value);
        }

        return $NewArray;
    }

	// function to strip and trim input field
	// @param	string	$Value		- value to be parsed, trimmed, etc.
	// @return	string	$Value		- parsed value
	static public function GetField($Value)
	{
		return trim(strip_tags($Value));
	}

	// function to swap two variables (arrays supported)
	// @param	string	$Var1		- first value to be swapped
	// @param	string	$Var2		- second value to be swapped
	// @return	boolean				- true
	static public function Swap(&$Var1, &$Var2)
	{
		list($Var2, $Var1) = array($Var1, $Var2);

		return true;
	}

	// check first is user logged in or not
	// @return	boolean
	//			true - those parameters set up properly
	//			false - they are not set up
	static public function CheckSession()
	{
		// check if username and usertype set in the session
		$Flag = true;
		if (!(isset($_SESSION['user']['username']) && !empty($_SESSION['user']['username']) && isset($_SESSION['user']['type']) && !empty($_SESSION['user']['type']))) {$Flag = false;}

		return $Flag;
	}

	// Get short string without breaking words
	// @param	string	$String		- string to be shorten
	// @param	string	$Counter	- counter, how many characters to be kept
	// @param	string	$Separator	- separator which will be put in the string
	// @return	string	$NewString	- parsed value
	static public function GetShortString($String, $Counter=100, $Separator='~~~')
	{
		$NewString = implode(array_slice(explode($Separator, wordwrap(str_replace(array("\n","\t","\r"),' ', $String), $Counter, $Separator)), 0, 1));
		if (strlen($NewString) != strlen($String)) {$NewString .= '...';}

		return $NewString;
	}

	// function to check if file exists on S3 or not
	// @param	string	$Url		- URL to the file which will be checked
	// @return	boolean	$Flag		- parsed value
	//			true - file exists
	//			false - file doesn't exist
	static public function FileExists($Url)
	{
		$FileHeader = @get_headers($Url);
		if ($FileHeader[0] == 'HTTP/1.1 200 OK'){$Flag = true;} else {$Flag = false;}

		return $Flag;
	}

	// function to build client-related path
	// @param	string	$Path		- existing 'relative' path
	// @return	string	$NewPath	- parsed real path
	static public function BuildClientPath($Path)
	{
		$NewPath = [];
		$Path = (array)($Path);
		foreach($Path as $Value)
		{
			$NewPath[] = str_replace('<client>', $_SESSION['url']['client'], $Value);
		}

		return $NewPath;
	}
}
?>