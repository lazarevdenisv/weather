<?
// Dispatcher class
class Dispatcher
{
	// constructor
	public function __construct()
	{
		exit;
	}

	// Dispatcher method
	public static function Dispatch()
	{
		$Url = ltrim(rtrim($_SERVER['REQUEST_URI'], '/'), '/');
		$Actions = explode('/', $Url);

		$_SESSION['url'] = [
			'controller'	=> ((isset($Actions[0]) && !empty($Actions[0])) ? $Actions[0] : DEFAULT_CONTROLLER),
//			'language'		=> ((isset($Actions[1])) ? $Actions[1] : DEFAULT_LANGUAGE),
			'action'		=> ((isset($Actions[1]) && !empty($Actions[1])) ? $Actions[1] : DEFAULT_ACTION),
			'params'		=> array_slice($Actions, 2),
		];
		
		$Params = $_SESSION['url']['params'];
		while (count($Params) > 0)
		{
			$_SESSION['url']['data'][array_shift($Params)] = array_shift($Params);
		}

		$_SESSION['data'] = Util::GetArray($_POST);

		$Acl = new ACL();
		if (!$Acl->CheckPermissions())
		{
			$_SESSION['url']['controller']	= DEFAULT_CONTROLLER;
			$_SESSION['url']['action']		= DEFAULT_ACTION;
			$_SESSION['url']['params']		= [];
		}

		return true;
	}

	// method to load proper controller
	public static function LoadController()
	{
		$Controller = ucfirst($_SESSION['url']['controller']).'Controller';

		require_once($_SESSION['url']['controller'].'.controller.php');

		new $Controller;

		return true;
	}
}
?>