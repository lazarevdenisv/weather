<?
// encryption
define("CRYPT_SALT",		'$$');
define("SALT_KEY",			'$6$rounds=10000');

// server config
define('URL_DOMAIN',		'http://denvlazarev.com/');
define("APP_ROOT",			dirname(__FILE__).'/');
define("PATH_CSS",			URL_DOMAIN.'public/css/');
define("PATH_JS",			URL_DOMAIN.'public/js/');
define("PATH_IMAGES",		URL_DOMAIN.'public/images/');
define("PATH_FONTS",		URL_DOMAIN.'public/fonts/');
define("PATH_DOCS",			URL_DOMAIN.'public/docs/');
//define("USER_IMAGES_PATH",	dirname(__FILE__).'/temp/user_images/');

// DB config
define('DB_HOST',			'localhost');
define('DB_DATABASE',		'');
define('DB_USERNAME',		'');
define('DB_PASSWORD',		'');

// Mail config
define('MAIL_HOST',			'');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
?>