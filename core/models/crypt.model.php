<?
// Crypt Model
class Crypt extends AbstractModel
{
	// constructor
	public function __construct()
	{
		if ($this->CheckCryptMethod() !== true) {echo "Sorry, system doesn't support SHA-512 crypting method";}

		return true;
	}

	// Checking Crypt Method, if system support SHA-512 or not
	// @param	none
	// @return	boolean
	//		true - SHA-512 method enabled in the system
	//		false - SHA-512 method disabled in the system
	private function CheckCryptMethod()
	{
		$Flag = (CRYPT_SHA512 == 1) ? true : false;

		return $Flag;
	}

	// Creating a hash for the password, crypt password using salt and return the result
	// @param	string	$UserPassword	- password that user entered
	// @return	string	$Password -
	//		<string> - crypted password
	//		null - password cannot be crypted
	public function CreateHash($UserPassword)
	{
		$Password = substr(crypt($UserPassword, SALT_KEY.CRYPT_SALT), 33);
		if (strlen($Password) != 86) {$Password = null;}

		return $Password;
	}
}
?>