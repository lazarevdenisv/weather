<?
// Model to connect with DB
class DBModel
{
	use SingletonTrait;

	public $Connection;
	public $Result;

	// protection againt new DB
	// @param	none
	// @return	boolean	- true
	private function __construct()
	{
		$this->Connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if ($this->Connection->connect_error)
		{
			die("Sorry, cannot connect to database<br /> Error No - ".
				$this->Connection->connect_errno.";<br /> Message - ".
				$this->Connection->connect_error);
		}
		mysqli_set_charset($this->Connection, "utf8");

		return true;
	}

	// create an instance of DB 
	// @param	none
	// @return	object	$Instance	- existing instance of DB connection
	public static function DBConnect()
	{
		static::$Instance = static::GetInstance();
		
		return static::$Instance;
	}

	// method to start MySQL profiler
	// @param	none
	// @return	boolean	- true
	public function StartProfiler()
	{
		$this->Result = @$this->Connection->query("set profiling = 1");
		
		return true;
	}

	// method to stop MySQL profiler
	// @param	none
	// @return	boolean	- true
	public function StopProfiler()
	{
		$this->Result = @$this->Connection->query("set profiling = 0");
		
		return true;
	}

	// method to get MySQL profiler data
	// @param	none
	// @return	array	- all records from profiler
	public function GetProfiler()
	{
		$this->Result = @$this->Connection->query("show profiles");

		return $this->FetchAll();
	}

	// method to start MySQL logging
	// @param	none
	// @return	boolean	- true
	public function StartLogging()
	{
		$this->Result = @$this->Connection->query("set global general_log = 'ON'");
		
		return true;
	}

	// method to stop MySQL logging
	// @param	none
	// @return	boolean	- true
	public function StopLogging()
	{
		$this->Result = @$this->Connection->query("set global general_log = 'OFF'");
				
		return true;
	}

	// method to run SELECT query against DB
	// @param	string	$Query	- query to be run
	// @return	array	$Result	- all records
	public function Select($Query)
	{
		$this->Result = @$this->Connection->query($Query);
		$Result = $this->FetchAll();
		
		return $Result;
	}

	// method to run other type of queries (beside SELECT) against DB
	// @param	string	$Query	- query to be run
	// @return	array	$Result	- set of results:
	//		- boolean	SUCCESS
	//				- true, if query was run properly
	//				- false, if we experiences an error
	//		- int		LAST_INSERT_ID	-	last_id for the last inserted query (if it was INSERT)
	//		- int		AFFECTED_ROWS	-	number of DB rows, affected by the query
	public function Query($Query)
	{
		$Result = ['success' => false, 'affected_rows' => 0, 'last_insert_id' => null];
		$Result['success'] = @$this->Connection->query($Query);
		if($Result['success'])
		{
			$Result['last_insert_id'] = @$this->Connection->insert_id;
			$Result['affected_rows'] = @$this->Connection->affected_rows;
		}
		
		return $Result;
	}

	// method to get last inserted ID
	// @param	none
	// @return	int		$ID	- ID from the last query
	public function LastInsertID()
	{
		$ID = @$this->Connection->insert_id;
		
		return $ID;
	}

	// method to get real_escape_string
	// @param	string	$String			- input string for the query
	// @return	string	$EscapedString	- result, escaped string
	public function EscapeString($String)
	{
		$EscapedString = $this->Connection->real_escape_string($String);
		
		return $EscapedString;
	}

	// method to fetch all records
	// @param	boolean	$AutoFreeResult	- flag to free result after SQL queries
	//			- true (default), free result
	//			- false, don't free result
	// @return	array	$Rows			- all rows, which were returned from SQL queries
	public function FetchAll($AutoFreeResult=true)
	{
		$Rows = null;
		// check if native mysqli fetch_all method works
		if(method_exists($this->Result, 'fetch_all'))
		{
			// then use it
			$Rows = @$this->Result->fetch_all(MYSQLI_ASSOC);
		}
		else
		{
			// otherwise, manually loop through the result and build dataset
			$Index = 0;
			$SingleRow = null;
			while($SingleRow = @$this->Result->fetch_assoc())
			{
				if($SingleRow !== null)
				{
					$Rows[$Index] = $SingleRow;
					$Index++;
				}
			}
		}
		// free result if necessary
		if($Rows !== null && $AutoFreeResult)
		{
			@$this->Result->free();
		}
		
		return $Rows;
	}
}
?>