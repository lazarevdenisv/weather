<?
// ABSTRACT model, main model for the whole project, all other models will extend it
abstract class AbstractModel
{
	protected $Db;

	// constructor
	public function __construct()
	{
		$this->Db = $_SESSION['db'] = DBModel::DBConnect();
		$this->SessionURL	= &$_SESSION['url'];
		$this->SessionData	= &$_SESSION['data'];

		$this->Post = Util::GetArray($_POST);
		$this->Get = Util::GetArray($_GET);
	}

	// method GetWhereParams, build WHERE params
	// @param	array	$Params	- set of params to parse it to WHERE string
	// @return	string	$Result	- returns properly constructed WHERE string
	public function GetWhereParams($Params)
	{
		$Result = array();

		foreach ($Params as $Key => $Value)
		{
//			$Result[] = $Key.' = '.$Value;
			$Result[] = $Key." = '".$this->Db->EscapeString($Value)."'";
		}

		return $Result;
	}

	// method GetOrderParams, build ORDER params
	// @param	array	$Params	- set of params to parse it to ORDER string
	// @return	string	$Result	- returns properly constructed ORDER string
	public function GetOrderParams($Params)
	{
		$Result = [];

		foreach ($Params as $Key => $Value)
		{
			$Result[] = $Key.' '.$Value;
		}

		return $Result;
	}}
?>