<?
// Service for About Controller
class HomeService extends AbstractService
{
	use MessageTrait;

	// constructor
	public function __construct()
	{
		parent::__construct();
	}

	// method GetWeather, getting weather forecast for the selected city
	// @param	none
	// @return	array	$Forecast	- returns forecast dataset
	public function GetWeather()
	{
		$Forecast = [];
		$Url = 'http://api.openweathermap.org/data/2.5/weather?q={city}&units=metric&appid=aea8dd97405ac8946626da5a71721e0a';
		$City = $this->Post['city'];
		$Url = str_replace('{city}', $City, $Url);

		$Request = curl_init();
		curl_setopt($Request, CURLOPT_URL, $Url);
		curl_setopt($Request, CURLOPT_RETURNTRANSFER, 1);
		$Result = curl_exec($Request);
		curl_close($Request);

		if (!empty($Result))
		{
			$Weather = json_decode($Result, true);

			$Forecast = [
				'temp'		=> $Weather['main']['temp'],
				'wind'		=> $Weather['wind']['speed'],
				'clouds'	=> $Weather['clouds']['all'],
			];
		}

		if ($Forecast['temp'] < 10)
		{
			$Forecast['jacket'] = true;
			$Forecast['jacket_reason'] = 'temperature is low (<10)';
		}
		elseif($Forecast['wind'] > 5 && $Forecast['temp'] < 15)
		{
			$Forecast['jacket'] = true;
			$Forecast['jacket_reason'] = 'wind is strong (>5) and temperature is not high enough (<15)';
		}
		elseif($Forecast['clouds'] > 50)
		{
			$Forecast['jacket'] = true;
			$Forecast['jacket_reason'] = 'it could be rainy (clouds>50)';
		}
		else
		{
			$Forecast['jacket'] = false;
		}

		return $Forecast;
	}
}
?>