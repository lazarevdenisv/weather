<?
// Abstract Service
abstract class AbstractService
{
	protected $ListingFields;
	protected $Validation;
	public $Model;
	protected $Fields;

	// constructor
	public function __construct()
	{
		$this->Validation = new Validation();
		$this->SessionUser	= &$_SESSION['user'];
		$this->SessionData	= &$_SESSION['data'];

		$this->Post = Util::GetArray($_POST);
		$this->Get = Util::GetArray($_GET);
	}

	// method SetModel, to set model for the controller
	// @param	object	$Model				- model to be set as a model for the controller
	// @param	string	$Name (optional)	- name of additional model
	//			- if $Name == null, then model sets as default model
	//			- if $Name set, then model sets as additional model
	// @return	boolean	- true
	public function SetModel($Model, $Name=null)
	{
		if (is_null($Name))
		{
			$this->Model = $Model;
		}
		else
		{
			$this->{$Name.'Model'} = $Model;
		}

		return true;
	}

	// method BuildListingHeader, build header for the listing
	// @param	string	$Field		- field name to be sorted
	// @param	string	$Direction	- sorting direction
	// @return	array	$Header		- data set of the header
	public function BuildListingHeader($Field, $Direction)
	{
/*
		$Header = [];

		foreach ($this->ListingFields as $Value1)
		{
			$Header[$Value1['field']] = [
				'title'	=> $this->Translation[$Value1['field']],
				'img'	=> (!$Value1['sort']) ? null : (($Value1['field'] != $Field) ? '' : (($Direction == 'asc') ? '-asc' : '-desc')),
				'data'	=> $Value1['data'],
			];
		}

		return $Header;
*/
	}

	// method GetErrorFields, build list of fields with errors
	// @param	array	$Fields			- array of fields to be passed as errors
	// @return	array	$FieldTitles	- list of human-readable titles for error fields
	protected function GetErrorFields($Fields)
	{
		$FieldTitles = [];
		foreach ($Fields as $Field)
		{
			$FieldTitles[] = $this->Fields[$Field];
		}

		return $FieldTitles;
	}
}
?>