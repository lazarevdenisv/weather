<?
// controller for HOME page
class HomeController extends AbstractController
{
	use MessageTrait;

	const	DEFAULT_ACTION = 'Index';
	private $HomeModel;
	private $Cities = ['London', 'Paris', 'Moscow', 'Madrid', 'Oslo', 'Revda', 'Marrakesh', 'Aberdeen', 'Keflavik'];

	// constructor
	public function __construct()
	{
		parent::__construct();

		$this->HeaderView	= 'header';
		$this->BodyView		= 'index';
		$this->FooterView	= 'footer';

		$this->Dispatch();
	}

	// method Dispatcher, do all presets & call proper ACTION
	// @param	none
	// @return	boolean	- true
	private function Dispatch()
	{
		$Action = (method_exists($this, ucwords($this->SessionURL['action']))) ? ucwords($this->SessionURL['action']) : self::DEFAULT_ACTION;
		$this->GetLastMessageFromSession();
		if ($this->Message['action'])
		{
			$this->TVars['message'] = [
				'message'	=> $this->Message['message'],
				'display'	=> 'block',
				'type'		=> ($this->Message['response'] == 'fail') ? 'message_error' : 'message_success',
			];
		}
		else
		{
			$this->TVars['message'] = [
				'message'	=> '',
				'display'	=> 'none',
				'type'		=> '',
			];
		}

		$this->$Action();

		return true;
	}

	// action - INDEX (default action)
	// @param    none
	// @return    boolean    - true
	private function Index()
	{
		$this->TVars['page']['cities'] = $this->Cities;

		if (isset($this->Post['city']))
		{
			$this->TVars['page']['city'] = $this->Post['city'];
			$this->TVars['page']['weather'] = $this->Service->GetWeather();
		}
		else
		{
			$this->TVars['page']['city'] = $this->Cities[0];
		}

		$this->LoadView();
		$this->ResetMessage();

		return true;
	}
}
?>