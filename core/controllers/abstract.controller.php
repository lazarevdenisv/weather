<?
// ABSTRACT controller, main controller, which extends all other controllers
abstract class AbstractController
{
	protected $HeaderView;
	protected $BodyView;
	protected $FooterView;

	protected $SessionURL;
	protected $SessionUser;
	protected $Service;
	protected $Controller;
	protected $Action;
	protected $Language;
	protected $LanguageID;

//	protected $TranslationModel;
//	protected $Translation = [];
	protected $TVars = [];
	protected $CurrentID = null;

	protected $Post;
	protected $Get;

	// constructor
	public function __construct()
	{
		$this->SessionURL	= &$_SESSION['url'];
		$this->SessionData	= &$_SESSION['data'];
		$_SESSION['user']	= (isset($_SESSION['user'])) ? $_SESSION['user'] : [];
		$this->SessionUser	= &$_SESSION['user'];
		$this->Controller	= $this->SessionURL['controller'];
		$this->Action		= $this->SessionURL['action'];
		if (class_exists(str_replace('Controller', 'Service', get_class($this))))
		{
			$ServiceName = str_replace('Controller', 'Service', get_class($this));
			$this->Service = new $ServiceName();
		}
		$this->Post = Util::GetArray($_POST);
        $this->Get = Util::GetArray($_GET);
	}

	// method to load proper view
	// @param	none
	// @return	boolean	- true
	protected function LoadView()
	{
		$this->LoadHeader();
		$this->LoadBody();
		$this->LoadFooter();

		return true;
	}

	// method to load proper header
	// @param	none
	// @return	boolean	- true
	protected function LoadHeader()
	{
		if (!empty($this->HeaderView)) {include($this->HeaderView.'.phtml');}

		return true;
	}

	// method to load proper body
	// @param	none
	// @return	boolean	- true
	protected function LoadBody()
	{
		if (!empty($this->BodyView)) {include($this->Controller.'/'.$this->BodyView.'.phtml');}

		return true;
	}

	// method to load proper footer
	// @param	none
	// @return	boolean	- true
	protected function LoadFooter()
	{
		if (!empty($this->FooterView)) {include($this->FooterView.'.phtml');}

		return true;
	}
}
?>