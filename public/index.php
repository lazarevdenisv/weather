<?
	session_start();

	// generic includes
	require_once('../libs/includes.php');

	// dispatch the request
	Dispatcher::Dispatch();

	// check if it's an exception or not
	if (!isset($Exceptions) || !in_array($_SESSION['url']['controller'], $Exceptions))
	{
		// it's not an exception, parse it
		set_include_path(implode(PATH_SEPARATOR, $Path));

		// load config
		require_once('../conf/config.php');

		// preset autoloaders
		//	$_SESSION['js'] = $JSScripts;
		$AutoLoaders = ['Controller', 'Model', 'Trait', 'Service'];

		// run autoloader walkthrough
		spl_autoload_extensions('.php,.controller.php,.model.php,.trait.php,.service.php');
		spl_autoload_register(
			function($ClassName)
			{
				global $AutoLoaders;

				// load public controllers & models: home.controller.php = HomeController class
				foreach ($AutoLoaders as $Folder)
				{
					spl_autoload(strtolower(str_replace($Folder, '', $ClassName)));
				}
			}
		);

		// preset headers
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Credentials: true');
		header('Content-type: text/html; charset=utf-8');

		// load proper controller
		Dispatcher::LoadController();

		// check if we opened MYSQL profiler & logger, then stop them
		if (MYSQL_PROFILER === true) {$_SESSION['db']->StopProfiler();}
		if (MYSQL_LOGGING === true) {$_SESSION['db']->StopLogging();}
	}
	else
	{
		// it's an exception, run it as is
		require_once (APP_ROOT.$_SERVER['REQUEST_URI']);
	}
?>
